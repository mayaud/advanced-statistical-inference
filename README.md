# Travaux Pratiques Cloud avec Azure

Ce dépôt contient les travaux pratiques (TPs) réalisés dans le cadre de la formation sur le cloud computing utilisant Microsoft Azure. Ces TPs visent à fournir une compréhension pratique des concepts et des services cloud en exécutant des applications réelles et en gérant l'infrastructure sur Azure.

## Objectif des TPs

L'objectif de ces TPs est d'initier les étudiants aux divers aspects du cloud computing, y compris la mise en œuvre de microservices, la gestion de la charge, l'équilibrage de charge, l'automatisation du déploiement, et plus encore. Chaque TP couvre des concepts spécifiques discutés lors des cours et fournit une expérience pratique de leur application dans un environnement cloud réel.

### TP1: Microservice de Calcul d'Intégrale Numérique

Le premier TP implique la création d'un microservice en Flask pour le calcul d'intégrales numériques. Les étudiants déploient ce service dans le cloud Azure, où ils apprennent à gérer la disponibilité et la scalabilité du service.

### TP2: Expérience de Tirage de Pièces

Le deuxième TP conduit les étudiants à travers la mise en œuvre d'une expérience de tirage de pièces en utilisant le service Azure Functions. Ce TP se concentre sur la compréhension de la mise en œuvre de concepts statistiques et leur automatisation dans le cloud.

### TP3: Régression Linéaire Bayésienne

Le troisième TP introduit les concepts de l'apprentissage automatique dans le cloud à l'aide de Azure Machine Learning. Les étudiants construisent et déploient un modèle de régression linéaire bayésienne et explorent les performances du modèle dans le cloud.





